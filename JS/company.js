'use strict';
//load companies
let companyList = [];

function init() {
    let totalLoader = document.querySelector('#total-loader');
    totalLoader.classList.add('loader');

    let companyLoader = document.querySelector('#company-loader');
    companyLoader.classList.add('loader');
    let companyListWrap = document.querySelector('.list-company');
    let locationLoader = document.querySelector('#location-loader');
    locationLoader.classList.add('loader');

    fetch('http://codeit.pro/codeitCandidates/serverFrontendTest/company/getList')
        .then((response) => {return response.json();})
        .then((data) => {
            totalLoader.classList.remove('loader');
            companyLoader.classList.remove('loader');
            locationLoader.classList.remove('loader');
            companyListWrap.classList.remove('list-company-hide');         
            companyList = data.list;
            createCompanyNameList();
            getTotalCompany();
            countLocation();

        })
        .catch((error) => {console.error(error)})
}

init();

function getTotalCompany() {
    let totalCompany = companyList.length,
        totalCompanyWrap = document.querySelector('.total-company'),
        countWrap = document.createElement('div');
    countWrap.classList.add('count-wrap');
  
    let countCompany = document.createElement('p');
    countCompany.classList.add('count-company');
    countCompany.textContent = totalCompany;
    countWrap.appendChild(countCompany);
    totalCompanyWrap.appendChild(countWrap);
}

function createCompanyNameList() {
    let companies = document.querySelector('.list-company');
    companyList.forEach((company) => {
        let companyItem = document.createElement('li');

        companyItem.classList.add('list-company-item');

        companyItem.textContent = company.name;
        companies.appendChild(companyItem);
        companyItem.addEventListener('click', () => {
            showPartners(company.partners);
        });
    });
}

//create partners list
function showPartners(partners) {
    let companyPartners = document.querySelector('.company-partners');
    companyPartners.classList.add('company-partners-show');
    let partnerItemWrap = document.querySelector('.company-partners-item-wrap');
    partnerItemWrap.innerHTML = '';

    partners.sort(function (a, b) {
        if(a.name > b.name) {
            return 1;
        }
        if(a.name < b.name) {
            return -1;
        }
        return 0;
      });

    partners.forEach((partner) => {
        let partnerItem = document.createElement('div');
        partnerItem.classList.add('company-partners-item');

        let p = document.createElement('p');
        p.classList.add('partner-name');
        p.textContent = partner.name;

        let div = document.createElement('div');
        div.classList.add('vertical-line');

        let span = document.createElement('span');
        span.classList.add('circle-percent');

        let percent = document.createElement('p');
        percent.textContent = partner.value + '%';

        span.appendChild(percent);
        div.appendChild(span);
        partnerItem.appendChild(p);
        partnerItem.appendChild(div);
        partnerItemWrap.appendChild(partnerItem);
    })
}

function countLocation() {
    let locations = companyList.map((company) => {return company.location.name;}),
    reduceLocations = locations.reduce((prev, curr) => (prev[curr] = ++prev[curr] || 1, prev), {}),
    entriesLocations = Object.entries(reduceLocations);
     
    function createPieDiagram () {
        let data = entriesLocations,
        chart = anychart.pie();
 
        chart.data(data);
        chart.container('container');
        chart.draw();
   };
 
   createPieDiagram();   
 }

//load news
let newsList = [];

function news() {
    let newsLoader = document.querySelector('#news-loader');
    newsLoader.classList.add('loader');
    let sliderControl = document.querySelectorAll('.slider-control');
    sliderControl.forEach((control) => {
        control.classList.add('hide-slider-control');
    })
    
    fetch('http://codeit.pro/codeitCandidates/serverFrontendTest/news/getList')
        .then((response) => {return response.json();})
        .then((data) => {
            newsLoader.classList.remove('loader');
            sliderControl.forEach((control) => {
                control.classList.remove('hide-slider-control');
            })
            newsList = data.list;
            showNews(newsList);
            multiItemSlider('.slider');
        })
        .catch((error) => {console.error(error)})
}

news();

//create news list
function showNews(newsList) {
    let newsAboutCompany = document.querySelector('.slider-wrapper');

    newsList.forEach((news) => {
        let sliderItem = document.createElement('div');
        sliderItem.classList.add('slider-item');

        let sliderItemStyle = document.createElement('div');
        sliderItemStyle.classList.add('slider-item-style');

        let newsItems = document.createElement('div');
        newsItems.classList.add('news');

        let newsItemFirst = document.createElement('div');
        newsItemFirst.classList.add('news-item1');
        let newsImg = document.createElement('img');

        newsImg.src = news.img;
        newsImg.alt = 'News img';

        newsItemFirst.appendChild(newsImg);

        let publicWrap = document.createElement('div');
        publicWrap.classList.add('public-wrap');

        let authorName = document.createElement('p');
        authorName.classList.add('public-inf');
        authorName.textContent = 'Author:';
        let spanAuthor = document.createElement('span');
        spanAuthor.textContent = news.author;

        authorName.appendChild(spanAuthor);
        publicWrap.appendChild(authorName);

        let publicDate = document.createElement('p');
        publicDate.classList.add('public-inf');
        publicDate.textContent = 'Public:';
        let spanPublic = document.createElement('span');
        let date = new Date(news.date * 1000);
        spanPublic.textContent = date.toLocaleDateString("en-GB");

        publicDate.appendChild(spanPublic);
        publicWrap.appendChild(publicDate);
        newsItemFirst.appendChild(publicWrap);

        let newsItemSecond = document.createElement('div');
        newsItemSecond.classList.add('news-item2');

        let newsLink = document.createElement('a');
        newsLink.classList.add('news-title');
        newsLink.href = news.link;

        let newsDescription = document.createElement('p');
        newsDescription.textContent = news.description.length > 260
        ? news.description.substring(0, 260) + '...'
        : news.description;

        newsItemSecond.appendChild(newsLink);
        newsItemSecond.appendChild(newsDescription);

        newsItems.appendChild(newsItemFirst);
        newsItems.appendChild(newsItemSecond);
        sliderItemStyle.appendChild(newsItems);
        sliderItem.appendChild(sliderItemStyle);

        newsAboutCompany.appendChild(sliderItem);
    });
}

//define slider functionality
let multiItemSlider = (function () {
	return function(selector) {
	    let mainElement = document.querySelector(selector), 
            sliderWrapper = mainElement.querySelector('.slider-wrapper'),
            sliderItems = mainElement.querySelectorAll('.slider-item'),
            sliderControls = mainElement.querySelectorAll('.slider-control'),
            wrapperWidth = parseFloat(getComputedStyle(sliderWrapper).width),
            itemWidth = parseFloat(getComputedStyle(sliderItems[0]).width),  
            positionLeftItem = 0,
            transform = 0,
            step = itemWidth / wrapperWidth * 100,
            items = [];

            sliderItems.forEach(function (item, index) {
                items.push({ item: item, position: index, transform: 0 });
            });

	    let position = {
            getMin: 0,
            getMax: items.length - 1,
        }

        let transformItem = function(direction) {
            if(direction === 'right') {
                if((positionLeftItem + wrapperWidth / itemWidth - 1) >= position.getMax) {
                    return;
                }
                positionLeftItem++;
                transform -= step;
            }
            if(direction === 'left') {
                if(positionLeftItem <= position.getMin) {
                    return;
                }
                positionLeftItem--;
                transform += step;
            }
            sliderWrapper.style.transform = 'translateX(' + transform + '%)';
        }

        let controlClick = function () {
            let direction = this.classList.contains('slider-control-right') ? 'right' : 'left';
            transformItem(direction);
        };

        let setUpListeners = function () {
            sliderControls.forEach(function (item) {
            item.addEventListener('click', controlClick);
            });
        }

        setUpListeners();

        return {
            right: function () {
            transformItem('right');
            },
            left: function () {
            transformItem('left');
            }
        }
	}
  }());
