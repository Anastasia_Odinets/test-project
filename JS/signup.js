'use strict'

const userName = document.querySelector('#user-name-input'),
    userLastName = document.querySelector('#user-last-name-input'),
    userEmail = document.querySelector('#user-email-input'),
    userGender = document.querySelector('.select-gender'),
    userPassword = document.querySelector('#user-password-input'),
    userAgreement = document.querySelector('.check-agree'),
    sendBtn = document.querySelector('.sign-up-send'),
    errorUserName = document.querySelector('.error-user-name'),
    errorUserLastName = document.querySelector('.error-user-last-name'),
    errorEmail = document.querySelector('.error-email'),
    errorChooseGender = document.querySelector('.error-choose-gender'),
    errorPassword = document.querySelector('.error-password'),
    errorAgreement = document.querySelector('.error-agreement');

sendBtn.addEventListener('click', () => {
    if(validate()) {
        let auth = new AuthService();
        auth.login(userName.value, userLastName.value, userEmail.value, userGender.value, userPassword.value);
        let formInput = document.querySelectorAll('.form-input');
        formInput.forEach((input) =>{
            input.value = '';
        });
        let selectGender = document.querySelector('.select-gender');
        selectGender.value = '';
        let checkAgree = document.querySelector('.check-agree');
        checkAgree.checked = false;
    } else {
        console.log('error');
    }
});

//validate authentication form
function validate () {
    let isValue = true,
        lettersOnlyRegEx =  /^[a-zA-Zа-яА-ЯёЁ'][a-zA-Z-а-яА-ЯёЁ' ]+[a-zA-Zа-яА-ЯёЁ']?$/;
    
    errorUserName.innerHTML = '';
    if(userName.value === "" || userName.value == null) {
        isValue = false;
        let error = document.createElement('p');
        error.textContent = '* User name should not be empty';
        errorUserName.appendChild(error);
    } else if(!userName.value.match(lettersOnlyRegEx)) {
        isValue = false;
        let error = document.createElement('p');
        error.textContent = '* User name should not contain special symbols';
        errorUserName.appendChild(error);
    }

    errorUserLastName.innerHTML = '';
    if(userLastName.value === "" || userLastName.value == null) {
        isValue = false;
        let error = document.createElement('p');
        error.textContent = '* User last name should not be empty';
        errorUserLastName.appendChild(error);
    } else if(!userLastName.value.match(lettersOnlyRegEx)) {
        isValue = false;
        let error = document.createElement('p');
        error.textContent = '* User last name should not contain special symbols';
        errorUserLastName.appendChild(error);
    }

    let emailRegEx =  /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

    errorEmail.innerHTML = '';
    if(userEmail.value === "" || userEmail.value == null) {
        isValue = false;
        let error = document.createElement('p');
        error.textContent = '* User email should not be empty';
        errorEmail.appendChild(error);
    } else if(!userEmail.value.match(emailRegEx)) {
        isValue = false;
        let error = document.createElement('p');
        error.textContent = '* Wrong email format';
        errorEmail.appendChild(error);
    }

    errorChooseGender.innerHTML = '';
    if(userGender.value === ''){
        isValue = false;
        let error = document.createElement('p');
        error.textContent = '* Choose gender';
        errorChooseGender.appendChild(error);
    }

    let passwordRegEx = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{8,}$/;
    errorPassword.innerHTML = '';
    if(userPassword.value === "" || userPassword.value == null) {
        isValue = false;
        let error = document.createElement('p');
        error.textContent = '* Password should not be empty';
        errorPassword.appendChild(error);
    } else if(!userPassword.value.match(passwordRegEx)){
        let error = document.createElement('p');
        error.textContent = '* Password not strong enough';
        errorPassword.appendChild(error);
    }

    errorAgreement.innerHTML = '';
    if(!userAgreement.checked) {
        isValue = false;
        let error = document.createElement('p');
        error.textContent = '* Accept agreement';
        errorAgreement.appendChild(error);   
    }

    return isValue;
}

//helper classes for work with http requests
class Http {
    post(data) {
        return new Promise((resolve, reject) => {
            fetch('http://codeit.pro/codeitCandidates/serverFrontendTest/user/registration', {
                method: 'POST',
                mode: 'no-cors',
                body: JSON.stringify(data),
                headers: {
                    'Content-type': 'application/json'
                }
            })
            .then((response) => response.json)
            .then((data) => resolve(data))
            .catch((err) => reject(err))
        })
    }
}

class AuthService {
    login(name, secondname, email, gender, pass){
        const http = new Http(),
            data = {name, secondname, email, gender, pass};
        http.post(data)
        .then((res) => {
            console.log(res)
        })
    }
}
